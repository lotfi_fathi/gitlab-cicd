from flask import Flask, request

app = Flask(__name__)

@app.route('/')
def hello():
    return 'Hello'

@app.route('/calc')
def calc():
    a = request.args.get('a')
    b = request.args.get('b')
    return str(sum(int(a), int(b)))

def sum(a, b):
    return a + b 
